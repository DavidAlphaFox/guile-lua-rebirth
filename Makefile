GUILE := GUILE_AUTO_COMPILE=0 guile

.PHONY: test

run:
	@guile -L ./ --language=lua --no-auto-compile
test:
	@for t in ./test-suite/tests/*.test; do $(GUILE) -L ./ -L ./test-suite/ $$t; done
	@for t in ./test-suite/tests/lua/*.lua; do $(GUILE) -L ./ --language=lua $$t; done
